/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slg_events.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/26 12:55:35 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/03 18:51:03 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef SLG_EVENTS_H
# define SLG_EVENTS_H

# include "slg_render.h"

typedef enum e_move {
	LEFT,
	DOWN,
	UP,
	RIGHT
}	t_move;

t_tile	*tile(t_game *game, size_t col, size_t row);
void	slg_run_hooks(t_fov *fov);

#endif
