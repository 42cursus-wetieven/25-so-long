/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slg_map_builders.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/15 08:24:47 by wetieven          #+#    #+#             */
/*   Updated: 2021/10/20 16:36:26 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef SLG_MAP_BUILDERS_H
# define SLG_MAP_BUILDERS_H

t_error	put_void(t_game *game);
t_error	put_wall(t_game *game);
t_error	put_plyr(t_game *game);
t_error	put_tokn(t_game *game);
t_error	put_exit(t_game *game);

#endif
