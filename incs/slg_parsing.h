/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slg_parsing.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/09 19:29:16 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/04 12:35:03 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef SLG_PARSING_H
# define SLG_PARSING_H

# include "so_long.h"

typedef t_error	(*t_map_builder)(t_game *game);

typedef struct s_pars_swtch {
	const char		flag;
	t_map_builder	parser;
}	t_pars_swtch;

t_error	slg_parse_map(t_game *game, const char *map_path);

#endif
