/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slg_render.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 17:39:19 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/02 14:34:16 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef SLG_RENDER_H
# define SLG_RENDER_H

# include "so_long.h"
# include "slg_parsing.h"

int	slg_render_frame(t_fov *fov);

#endif
