/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slg_textures.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/26 09:20:59 by wetieven          #+#    #+#             */
/*   Updated: 2021/10/26 09:21:58 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef SLG_TEXTURES_H
# define SLG_TEXTURES_H

t_error	slg_erase_textures(t_fov *fov, t_tile erase_until, t_error cause);
t_error	mlx_get_txtr(char *path, t_txtr *texture, t_mlx mlx);
t_error	slg_parse_textures(t_fov *fov);

#endif
