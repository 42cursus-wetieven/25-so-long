/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 16:56:45 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/02 17:58:02 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef SO_LONG_H
# define SO_LONG_H

# include <stdbool.h>
# include "libft.h"

# define TILE_RANGE 5

typedef enum e_tile {
	VOID,
	WALL,
	PLYR,
	TOKN,
	EXIT
}	t_tile;

typedef struct s_plyr {
	bool	exists;
	t_pos	pos;
	size_t	steps;
}	t_plyr;

typedef struct s_game {
	t_map	map;
	t_plyr	plyr;
	size_t	tokens;
	bool	exit;
}	t_game;

typedef struct s_fov {
	t_mlx	mlx;
	size_t	height;
	size_t	width;
	t_img	*frm;
	size_t	tile_hgt;
	size_t	tile_wid;
	t_txtr	*txtr;
	t_game	*game;
}	t_fov;

t_error	slg_shutdown(t_fov *fov, t_error cause);

#endif
