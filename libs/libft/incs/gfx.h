/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gfx.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/30 13:39:09 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/03 18:52:17 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef GFX_H
# define GFX_H

# include <stddef.h>
# include "err.h"
# include "vct.h"

/*
** Rendering structures
*/
typedef struct s_mlx {
	void	*lnk;
	void	*win;
}	t_mlx;

typedef struct s_img {
	void	*ptr;
	int		*addr;
	int		bpp;
	int		line_size;
	int		endian;
}	t_img;

typedef struct s_txtr
{
	t_img	img;
	int		height;
	int		width;
}	t_txtr;

/*
** Game structures
*/
typedef struct s_pos {
	size_t	col;
	size_t	row;
}	t_pos;

typedef struct s_map {
	t_vctr	*grid;
	size_t	cols;
	size_t	rows;
}	t_map;

/* X11 keycodes
typedef enum e_xkeycodes {
	ESC_K = 61,
	LFT_K = 131,
	A_KEY = 8,
	DWN_K = 133,
	S_KEY = 9,
	UP_KY = 134,
	W_KEY = 21,
	RGT_K = 132,
	D_KEY = 10,
}	t_xkeycodes;
*/

typedef enum e_mlx_keycodes {
	ESC_K = 53,
	LFT_K = 123,
	A_KEY = 0,
	DWN_K = 125,
	S_KEY = 1,
	UP_KY = 126,
	W_KEY = 13,
	RGT_K = 124,
	D_KEY = 2,
}	t_mlx_keycodes;

void	set_pixel(t_img *fov, int x, int y, int color);
t_error	trns_to_int(int *color, int trans);
t_error	rgb_to_int(int *color, int r, int g, int b);

#endif
