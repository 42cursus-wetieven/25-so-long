#!/bin/bash

echo "../../so_long not_a_.ber_map"
../../so_long not_a_.ber_map
echo ""

echo "../../so_long not_a_valid_file_path.ber"
../../so_long not_a_valid_file_path.ber
echo ""

echo "cat too_low.ber"
cat too_low.ber
echo "../../so_long too_low.ber"
../../so_long too_low.ber
echo ""

echo "cat too_narrow.ber"
cat too_narrow.ber
echo "../../so_long too_narrow.ber"
../../so_long too_narrow.ber
echo ""

echo "cat too_small.ber"
cat too_small.ber
echo "../../so_long too_small.ber"
../../so_long too_small.ber
echo ""

echo "cat missing_stuff.ber"
cat missing_stuff.ber
echo "../../so_long missing_stuff.ber"
../../so_long missing_stuff.ber
echo ""

echo "cat invalid_item.ber"
cat invalid_item.ber
echo "../../so_long invalid_item.ber"
../../so_long invalid_item.ber
echo ""

echo "cat dented.ber"
cat dented.ber
echo "../../so_long dented.ber"
../../so_long dented.ber
echo ""
