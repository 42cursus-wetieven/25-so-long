/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slg_map_builders.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/15 06:50:03 by wetieven          #+#    #+#             */
/*   Updated: 2021/10/27 15:10:46 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "slg_parsing.h"

t_error	put_void(t_game *game)
{
	t_tile	tile;

	tile = VOID;
	return (vctr_push(game->map.grid, &tile));
}

t_error	put_wall(t_game *game)
{
	t_tile	tile;

	tile = WALL;
	return (vctr_push(game->map.grid, &tile));
}

t_error	put_plyr(t_game *game)
{
	game->plyr.exists = true;
	game->plyr.steps = 0;
	game->plyr.pos.row = game->map.rows;
	game->plyr.pos.col = game->map.grid->entries % game->map.cols;
	return (put_void(game));
}

t_error	put_tokn(t_game *game)
{
	t_tile	tile;

	tile = TOKN;
	if (vctr_push(game->map.grid, &tile) == MEM_ALLOC)
		return (MEM_ALLOC);
	game->tokens++;
	return (CLEAR);
}

t_error	put_exit(t_game *game)
{
	t_tile	tile;

	tile = EXIT;
	if (vctr_push(game->map.grid, &tile) == MEM_ALLOC)
		return (MEM_ALLOC);
	game->exit = true;
	return (CLEAR);
}
