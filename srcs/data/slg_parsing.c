/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slg_parsing.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 16:27:20 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/05 12:41:19 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "slg_parsing.h"
#include "slg_map_builders.h"
#include "slg_events.h"

static t_error	slg_map_chk(t_game *game)
{
	size_t	c;
	t_error	error;
	t_tile	this_tile;

	if (game->map.rows <= 2)
		error = ft_err_msg("Insufficient map height.", PARSE);
	else if (game->map.rows * game->map.cols < 15)
		error = ft_err_msg("The map is too small to hold the game.", PARSE);
	error = CLEAR;
	c = 0;
	this_tile = *tile(game, c, game->map.rows - 1);
	while (c < game->map.cols && this_tile == WALL)
	{
		c++;
		this_tile = *tile(game, c, game->map.rows - 1);
	}
	if (c != game->map.cols)
		error = ft_err_msg("There's a gap in your bottom wall.", PARSE);
	if (!game->tokens)
		error = ft_err_msg("A map needs tokens to be playable.", PARSE);
	if (!game->exit)
		error = ft_err_msg("A map needs an exit for this game to end.", PARSE);
	if (!game->plyr.exists)
		error = ft_err_msg("One can't play if there is no player.", PARSE);
	return (error);
}

static t_error	map_parser(t_game *game, const char map_elem)
{
	t_tile				i;
	static t_pars_swtch	parser_set[] = {
		[VOID] = {.flag = '0', .parser = &put_void},
		[WALL] = {.flag = '1', .parser = &put_wall},
		[PLYR] = {.flag = 'P', .parser = &put_plyr},
		[TOKN] = {.flag = 'C', .parser = &put_tokn},
		[EXIT] = {.flag = 'E', .parser = &put_exit}
	};

	i = 0;
	while (i < TILE_RANGE)
	{
		if (map_elem == parser_set[i].flag)
			return ((parser_set[i].parser)(game));
		i++;
	}
	ft_printf("Error\nInvalid flag \"%c\" on row %lu col %lu.\n",
		map_elem, game->map.rows,
		game->map.grid->entries % game->map.cols);
	return (PARSE);
}

static t_error	slg_build_row(t_game *game, const char *line_ptr)
{
	t_error	error;

	error = CLEAR;
	while (*line_ptr && !error)
	{
		error = map_parser(game, *line_ptr);
		if (game->map.rows == 0 && *line_ptr != '1')
			return (ft_err_msg("There's a gap in your top wall", PARSE));
		line_ptr++;
	}
	if (!error && *line_ptr == '\0' && *(line_ptr - 1) == '1')
		return (CLEAR);
	else
	{
		if (!error && *(line_ptr - 1) != '1')
			ft_printf("Error\nRow %lu : There's a gap in your right wall.\n",
				game->map.rows);
		return (PARSE);
	}
	return (CLEAR);
}

static t_error	slg_parse_row(t_game *game, const char *map_line,
		int gnl_status)
{
	t_error	error;

	if (gnl_status == 0 && !ft_strlen(map_line))
		return (CLEAR);
	if (!game->map.cols)
		game->map.cols = ft_strlen(map_line);
	if (!game->map.cols)
		return (ft_err_msg("Extraneous empty line opening map file", PARSE));
	if (game->map.cols <= 2)
		return (ft_err_msg("Insufficient map width.", PARSE));
	if (ft_strlen(map_line) != game->map.cols || *map_line == ' ')
		return (ft_err_msg("The map must be rectangular.", PARSE));
	if (*map_line != '1')
	{
		ft_printf("Error\nRow %lu : There's a gap in your left wall.\n",
			game->map.rows);
		return (PARSE);
	}
	error = slg_build_row(game, map_line);
	game->map.rows++;
	return (error);
}

// For correction and parsing tests :
/*
void	print_map_vctr(t_game *game, t_map map)
{
	size_t	c;
	size_t	r;

	r = 0;
	while (r < map.rows)
	{
		c = 0;
		while (c < map.cols)
			ft_printf("%i", *tile(game, c++, r));
		ft_printf("\n");
		r++;
	}
}
*/

t_error	slg_parse_map(t_game *game, const char *map_path)
{
	int		fd;
	char	*map_line;
	t_error	error;
	int		gnl_status;

	if (fd_opener(map_path, &fd) != CLEAR)
	{
		ft_printf("Error\nThe map file path \"%s\" leads nowhere\n", map_path);
		return (FD_OPENING);
	}
	error = CLEAR;
	while (!error)
	{
		gnl_status = get_next_line(fd, &map_line);
		if (gnl_status >= 0)
			error = slg_parse_row(game, map_line, gnl_status);
		free(map_line);
		if (gnl_status <= 0)
			break ;
	}
	fd_killer(fd);
	if (!error)
		error = slg_map_chk(game);
	return (error);
}
