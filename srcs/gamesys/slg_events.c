/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slg_events.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/26 10:48:02 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/04 13:01:15 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

//#include <X11/keysym.h>
#include "mlx.h"
#include "slg_events.h"
#include "slg_render.h"

int	slg_close_win(t_fov *fov)
{
	slg_shutdown(fov, CLEAR);
	return (CLEAR);
}

t_tile	*tile(t_game *game, size_t col, size_t row)
{
	return ((t_tile *)(game->map.grid->data + col * sizeof(t_tile)
		+ (row * game->map.cols) * sizeof(t_tile)));
}

static void	slg_move_plyr(t_game *game, t_fov *fov, t_move move)
{
	if (move == LEFT && game->plyr.pos.col > 0
		&& *tile(game, game->plyr.pos.col - 1, game->plyr.pos.row) != WALL)
		game->plyr.pos.col--;
	else if (move == DOWN && game->plyr.pos.row < game->map.rows - 1
		&& *tile(game, game->plyr.pos.col, game->plyr.pos.row + 1) != WALL)
		game->plyr.pos.row++;
	else if (move == UP && game->plyr.pos.row > 0
		&& *tile(game, game->plyr.pos.col, game->plyr.pos.row - 1) != WALL)
		game->plyr.pos.row--;
	else if (move == RIGHT && game->plyr.pos.col < game->map.cols - 1
		&& *tile(game, game->plyr.pos.col + 1, game->plyr.pos.row) != WALL)
		game->plyr.pos.col++;
	else
		return ;
	game->plyr.steps++;
	ft_printf("%lu steps\n", game->plyr.steps);
	if (*tile(game, game->plyr.pos.col, game->plyr.pos.row) == TOKN)
	{
		*tile(game, game->plyr.pos.col, game->plyr.pos.row) = VOID;
		game->tokens--;
	}
	else if (*tile(game, game->plyr.pos.col, game->plyr.pos.row) == EXIT
		&& !game->tokens)
		slg_shutdown(fov, CLEAR);
}

int	slg_key_press(int keycode, t_fov *fov)
{
	if (keycode == ESC_K)
		return (slg_close_win(fov));
	else if (keycode == A_KEY || keycode == LFT_K)
		slg_move_plyr(fov->game, fov, LEFT);
	else if (keycode == S_KEY || keycode == DWN_K)
		slg_move_plyr(fov->game, fov, DOWN);
	else if (keycode == W_KEY || keycode == UP_KY)
		slg_move_plyr(fov->game, fov, UP);
	else if (keycode == D_KEY || keycode == RGT_K)
		slg_move_plyr(fov->game, fov, RIGHT);
	return (0);
}

void	slg_run_hooks(t_fov *fov)
{
	mlx_hook(fov->mlx.win, 2, 1L << 0, slg_key_press, fov);
	mlx_hook(fov->mlx.win, 17, 1L << 17, slg_close_win, fov);
	mlx_loop_hook(fov->mlx.lnk, slg_render_frame, fov);
	mlx_loop(fov->mlx.lnk);
}
