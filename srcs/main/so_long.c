/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 10:59:30 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/04 14:21:22 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stdbool.h>
#include "mlx.h"
#include "so_long.h"
#include "slg_parsing.h"
#include "slg_textures.h"
#include "slg_events.h"
#include "slg_render.h"

t_error	slg_shutdown(t_fov *fov, t_error cause)

{
	if (cause == MEM_ALLOC)
		ft_printf("Error\nMemory is full, consider closing Google Chrome.\n");
	if (cause != FD_OPENING)
		vctr_exit(fov->game->map.grid);
	if (cause == CLEAR)
	{
		slg_erase_textures(fov, TILE_RANGE - 1, CLEAR);
		mlx_destroy_image(fov->mlx.lnk, fov->frm->ptr);
		mlx_destroy_window(fov->mlx.lnk, fov->mlx.win);
		if (!fov->game->tokens)
			ft_printf("You won in %lu steps !\n", fov->game->plyr.steps);
		else
			ft_printf("C'mon, don't you think this game is endearing ?\n");
		exit(CLEAR);
		return (CLEAR);
	}
	exit(ERROR);
	return (ERROR);
}

static t_error	slg_launch_fov(t_fov *fov)
{
	t_img	frm;

	fov->mlx.lnk = mlx_init();
	if (!fov->mlx.lnk)
		return (MEM_ALLOC);
	fov->mlx.win = mlx_new_window(fov->mlx.lnk, fov->width, fov->height,
			"so long, and thanks for all the dividends");
	if (!fov->mlx.win)
		return (MEM_ALLOC);
	fov->frm = &frm;
	fov->frm->ptr = mlx_new_image(fov->mlx.lnk, fov->width, fov->height);
	fov->frm->addr = (int *)mlx_get_data_addr(fov->frm->ptr, &fov->frm->bpp,
			&fov->frm->line_size, &fov->frm->endian);
	if (!fov->frm->ptr)
	{
		mlx_destroy_window(fov->mlx.lnk, fov->mlx.win);
		return (MEM_ALLOC);
	}
	if (slg_parse_textures(fov) == CLEAR)
		slg_run_hooks(fov);
	else
		return (PARSE);
	return (CLEAR);
}

static void	slg_scale_graphics(t_fov *fov, t_map *map)
{
	if (map->rows <= 28)
		fov->height = map->rows * 50;
	else
		fov->height = 1000;
	if (map->cols <= 40)
		fov->width = map->cols * 50;
	else
		fov->width = 1800;
	fov->tile_hgt = fov->height / map->rows;
	fov->tile_wid = fov->width / map->cols;
}

static void	slg_game_init(t_game *game)
{
	game->map.rows = 0;
	game->map.cols = 0;
	game->plyr.exists = false;
	game->tokens = 0;
	game->exit = false;
}

int	main(int ac, char **av)
{
	t_game	game;
	t_fov	fov;
	t_error	error;

	if (ac != 2)
	{
		ft_printf("USAGE : ./so_long <MAP_PATH>.ber\n");
		return (ERROR);
	}
	if (vctr_init(&game.map.grid, sizeof(t_tile), 15) != CLEAR)
		return (MEM_ALLOC);
	fov.game = &game;
	slg_game_init(&game);
	error = file_ext_chk(av[1], ".ber");
	if (error)
		ft_err_msg("Map file extension must be \".ber\"", PARSE);
	if (!error)
		error = slg_parse_map(&game, av[1]);
	if (!error)
		slg_scale_graphics(&fov, &game.map);
	if (!error)
		error = slg_launch_fov(&fov);
	return (slg_shutdown(&fov, error));
}
