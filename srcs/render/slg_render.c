/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slg_render.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 09:09:23 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/04 13:05:53 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "mlx.h"
#include "so_long.h"
#include "slg_events.h"

static void	draw_txtr(t_fov *fov, int col, int row, t_txtr texture)
{
	size_t	x;
	size_t	y;
	float	hor_scaling;
	float	ver_scaling;

	hor_scaling = (float)texture.width / (float)fov->tile_wid;
	ver_scaling = (float)texture.height / (float)fov->tile_hgt;
	y = 0;
	while (y < fov->tile_hgt)
	{
		x = 0;
		while (x < fov->tile_wid)
		{
			set_pixel(fov->frm, col + x, row + y,
				texture.img.addr[(int)(x * hor_scaling)
				+ (int)(y * ver_scaling) *texture.width]);
			x++;
		}
		y++;
	}
}

static int	slg_draw_map(t_fov *fov, t_map map)
{
	size_t	c;
	size_t	r;

	r = 0;
	while (r < map.rows)
	{
		c = 0;
		while (c < map.cols)
		{
			if (*tile(fov->game, c, r) == TOKN
				|| *tile(fov->game, c, r) == EXIT)
				draw_txtr(fov,
					c * fov->tile_wid, r * fov->tile_hgt, fov->txtr[VOID]);
			draw_txtr(fov, c * fov->tile_wid,
				r * fov->tile_hgt, fov->txtr[*tile(fov->game, c, r)]);
			c++;
		}
		r++;
	}
	return (CLEAR);
}

int	slg_render_frame(t_fov *fov)
{
	char	*steps;
	char	*step_msg;

	slg_draw_map(fov, fov->game->map);
	draw_txtr(fov, fov->game->plyr.pos.col * fov->tile_wid,
		fov->game->plyr.pos.row * fov->tile_hgt, fov->txtr[PLYR]);
	steps = ft_sztoa_base(fov->game->plyr.steps, "0123456789");
	step_msg = ft_strjoin("Steps : ", steps);
	free(steps);
	mlx_put_image_to_window(fov->mlx.lnk, fov->mlx.win, fov->frm->ptr, 0, 0);
	mlx_string_put(fov->mlx.lnk, fov->mlx.win, 5, 15, 0x00FFFFFF, step_msg);
	free(step_msg);
	return (CLEAR);
}
