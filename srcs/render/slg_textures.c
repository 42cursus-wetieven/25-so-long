/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slg_textures.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 19:20:10 by wetieven          #+#    #+#             */
/*   Updated: 2021/11/04 12:38:38 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "libft.h"
#include "mlx.h"
#include "so_long.h"
#include "slg_render.h"

t_error	slg_erase_textures(t_fov *fov, t_tile erase_until, t_error cause)
{
	t_tile	i;

	i = 0;
	while (i < erase_until)
		mlx_destroy_image(fov->mlx.lnk, fov->txtr[i++].img.ptr);
	if (cause == CLEAR)
		mlx_destroy_image(fov->mlx.lnk, fov->txtr[i].img.ptr);
	return (cause);
}

t_error	mlx_get_txtr(char *path, t_txtr *texture, t_mlx mlx)
{
	int	fd;

	if (fd_opener(path, &fd) != CLEAR)
	{
		ft_printf("\"%s\" does not exist, check your texture file path.\n",
			path);
		return (PARSE);
	}
	texture->img.ptr = mlx_xpm_file_to_image(mlx.lnk, path, &texture->width,
			&texture->height);
	if (texture->img.ptr == NULL)
	{
		ft_printf("\"%s\" isn't a valid xpm file.", path);
		return (PARSE);
	}
	texture->img.addr = (int *)mlx_get_data_addr(texture->img.ptr,
			&texture->img.bpp, &texture->img.line_size, &texture->img.endian);
	fd_killer(fd);
	return (CLEAR);
}

t_error	slg_parse_textures(t_fov *fov)
{
	static t_txtr	textures[TILE_RANGE];

	fov->txtr = textures;
	if (mlx_get_txtr("./textures/void.xpm", &fov->txtr[VOID], fov->mlx))
		return (PARSE);
	if (mlx_get_txtr("./textures/wall.xpm", &fov->txtr[WALL], fov->mlx))
		return (slg_erase_textures(fov, WALL, PARSE));
	if (mlx_get_txtr("./textures/plyr.xpm", &fov->txtr[PLYR], fov->mlx))
		return (slg_erase_textures(fov, PLYR, PARSE));
	if (mlx_get_txtr("./textures/tokn.xpm", &fov->txtr[TOKN], fov->mlx))
		return (slg_erase_textures(fov, TOKN, PARSE));
	if (mlx_get_txtr("./textures/exit.xpm", &fov->txtr[EXIT], fov->mlx))
		return (slg_erase_textures(fov, EXIT, PARSE));
	return (CLEAR);
}
